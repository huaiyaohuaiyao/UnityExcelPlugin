﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;
using System.Collections.Generic;

public class ClassCreateHelper
{

	[MenuItem("Data/ProtoDB Explorer", false, 89)]
    public static void General()
    {
        string configPath =  Application.streamingAssetsPath + "/DataConfig/DataConfig.txt";
        string txt = File.ReadAllText(configPath);
        List<object> lstContent = Json.Deserialize(txt) as List<object>;
        foreach (Dictionary<string, object> kv in lstContent)
        {
            string fileName = kv["ExcelName"].ToString();
            string className = kv["ClassName"].ToString();
            CreateCsFile(fileName, className);
            //CreateCsFile("tnt_deploy_goods_info", "GOODS_INFO");
        }
       // CreateCsFile("tnt_deploy_goods_info", "GOODS_INFO");
    }

    public static void CreateCsFile(string excelName,string className)
    {
        string fileName = "tnt_deploy_" + excelName;
        string tagCsFolder = Application.dataPath + "/Scripts/Killer/DataConfig/ProtoGen/";
        string tempPath = Application.dataPath + "/ExcelPlugin/Scripts/Base/template.txt";
        string tagCsPath = tagCsFolder + fileName + "_Ext.cs";

        if (!File.Exists(tagCsPath))
        {
            string txt = File.ReadAllText(tempPath);
            string retTxt = txt.Replace("{0}", className);

            CreateFile(tagCsPath, retTxt);
        }
        else
        {
            Debug.LogWarning(fileName + "_Ext.cs " + "is Exists");
        }
        
    }

    //创建文件
    public static void CreateFile(string path, string content)
    {
        FileStream fs1 = new FileStream(path, FileMode.Create, FileAccess.Write);//创建写入文件 
        StreamWriter sw = new StreamWriter(fs1);
        sw.WriteLine("");//开始写入值
        sw.Close();
        fs1.Close();

        File.WriteAllText(path, content);
    }
}
