﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Threading;
using System.Diagnostics;

public class ProcessBat
{

    static void ExcuteRebuildAtlas()
    {
        string path = Application.dataPath + "/../tools/kill.bat";
        UnityEngine.Debug.Log(path);
        RunBat(path, "hero HERO des");
    }

    /// <summary>
    /// 生成配置文件
    /// </summary>
    /// <param name="fileName"></param>
    /// <param name="className"></param>
    public static void create(string fileName,string className)
    {
        string path = Application.dataPath + "/../tools/kill.bat";
        UnityEngine.Debug.Log(path);
        RunBat(path, fileName + " " + className + " des");
        AssetDatabase.Refresh();
    }

    /// <summary>
    /// 执行cmd命令
    /// </summary>
    /// <param name="batPath"></param>
    /// <param name="arguments"></param>
    private static void RunBat(string batPath, string arguments)
    {
        Process pro = new Process();

        FileInfo file = new FileInfo(batPath);
        pro.StartInfo.WorkingDirectory = file.Directory.FullName;
        pro.StartInfo.FileName = batPath;
        pro.StartInfo.CreateNoWindow = false;
        pro.StartInfo.Arguments = arguments;
        pro.Start();
        pro.WaitForExit();
    }
}
