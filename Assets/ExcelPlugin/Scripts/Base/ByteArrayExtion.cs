﻿/*
* 作 者: 槐尧
* 创建时间: 2015/12/27 16:20:42
* 说明:  Byte数组的扩展
*/

using UnityEngine;
using System.Collections;

public static  class ByteArrayExtion
{
    static public string GetString(this byte[] value)
    {
        string str = System.Text.Encoding.UTF8.GetString(value);
        return str;
    }

    static public void SetString(this byte[] value,string str)
    {
        value = System.Text.Encoding.UTF8.GetBytes(str);
    }
	
}
