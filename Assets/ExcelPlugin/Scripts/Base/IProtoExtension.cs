﻿/*
* 作 者: 槐尧
* 创建时间: 2015/12/27 16:23:42
* 说明:  
*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace tnt_deploy
{
    public interface IProtoExtension
    {
        string GetId();
    }

    public interface IProtoArrayExtension
    {
        IProtoExtension GetItemByKey(object id);

        object GetItems() ;
    }
}