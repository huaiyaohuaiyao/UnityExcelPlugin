﻿/*
* 作 者: 槐尧
* 创建时间: 2015/12/27 15:23:42
* 说明:  
*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using ProtoBuf;
using System;
namespace tnt_deploy
{
    public class ProtoDatabaseConfig
    {
        public string excelName;
        public string className;
    }

    public class ProtoDatabase
    {
        static ProtoDatabase m_instance;
        private Dictionary<string, IProtoArrayExtension> _datas = null;

        public static ProtoDatabase Instance
        {
            get 
            {
                if (m_instance == null)
                {
                    m_instance = new ProtoDatabase();
                    m_instance.InitDatas();
                }
                
                return ProtoDatabase.m_instance; 
            }
        }

        /// <summary>
        /// 初始化数据
        /// </summary>
        void InitDatas()
        {
            if (_datas == null)
            {
                _datas = new Dictionary<string, IProtoArrayExtension>();
                string configPath = Application.streamingAssetsPath + "/DataConfig/DataConfig.txt";
                string content = File.ReadAllText(configPath);
                List<object> lstObj = Json.Deserialize(content) as List<object>;
                List<ProtoDatabaseConfig> ls = new List<ProtoDatabaseConfig>();

                foreach (object o in lstObj)
                {
                    Dictionary<string, object> kv = o as Dictionary<string, object>;

                    ProtoDatabaseConfig config = new ProtoDatabaseConfig();
                    config.excelName = "tnt_deploy_" + kv["ExcelName"].ToString();
                    config.className = kv["ClassName"].ToString();
                    ls.Add(config);
                }
                Init(ls);
            }
        }
        
        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="lst"></param>
        public void Init(List<ProtoDatabaseConfig> lst)
        {
            foreach(ProtoDatabaseConfig each in lst)
            {
                System.Type classType = System.Type.GetType("tnt_deploy." + each.className + "_ARRAY");
                IProtoArrayExtension read = ReadOneDataConfig(classType, each.excelName) as IProtoArrayExtension;
                _datas["tnt_deploy." + each.className] = read;
                //Debug.Log(read.GetItemByKey)
            }
        }

        /// <summary>
        /// 根据ID获取数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>
        /// <returns></returns>
        public T GetTypeAndId<T>(object id) where T : IProtoExtension
        {
            IProtoArrayExtension array = null;
            if (!_datas.TryGetValue(typeof(T).ToString(), out array)) return default(T);
            
            IProtoExtension item = array.GetItemByKey(id);
            return (T)item;

        }

        /// <summary>
        /// 获取所有该类型数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public List<T> GetData<T>() where T : IProtoExtension
        {
            IProtoArrayExtension array = null;
            if (!_datas.TryGetValue(typeof(T).ToString(), out array)) return null;
            return array.GetItems() as List<T>;
        }

        /// <summary>
        /// 读取数据
        /// </summary>
        /// <param name="type">类型</param>
        /// <param name="FileName">文件名</param>
        /// <returns></returns>
        private object ReadOneDataConfig(System.Type type,string FileName)
        {
            object o = null;
            FileStream fileStream;
            fileStream = GetDataFileStream(FileName);
            if (null != fileStream)
            {
                o = Serializer.Deserialize(type, fileStream);
                fileStream.Close();
                return o;
            }

            return o;
        }

        /// <summary>
        /// 读取数据
        /// </summary>
        /// <param name="type">类型</param>
        /// <param name="FileName">文件名</param>
        /// <returns></returns>
        private T ReadOneDataConfig<T>(string FileName)
        {
            FileStream fileStream;
            fileStream = GetDataFileStream(FileName);
            if (null != fileStream)
            {
                T t = Serializer.Deserialize<T>(fileStream);
                fileStream.Close();
                return t;
            }

            return default(T);
        }

        /// <summary>
        /// 读取数据流
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private FileStream GetDataFileStream(string fileName)
        {
            string filePath = GetDataConfigPath(fileName);
            if (File.Exists(filePath))
            {
                FileStream fileStream = new FileStream(filePath, FileMode.Open);
                return fileStream;
            }

            return null;
        }

        /// <summary>
        /// 根据文件名称获取文件路径名
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private string GetDataConfigPath(string fileName)
        {
            return Application.streamingAssetsPath + "/DataConfig/" + fileName + ".data";
        }
    }
}
