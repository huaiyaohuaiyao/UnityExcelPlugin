### **通用流程图**

![流程图](https://git.oschina.net/uploads/images/2016/0219/160849_26793774_99114.png "通用流程图")
**通用流程图简述**

以hello.xls为例

1. 在python运行时下，依赖proto组件、xlrd的组件，使用xls_deploy_tool.py处理Hello.xls，生成Hello.data数据文件、及其对应的Hello.proto解释类。
2. 在windows系统下，使用protoc.exe，将proto解释类转成中间格式Hello.desc。
3. 将中间格式Hello.desc用语言工具翻译成其他语言解释类比如c#解释类Hello.cs。
4. 将中间格式Hello.desc用语言工具翻译成其他语言解释类比如c#解释类Hello.cs。

**所需工具以及安装流程**

所需工具下载地址：链接：http://pan.baidu.com/s/1nunj7OP 密码：j00k

**安装python**

1.  安装python，并将路径添加到本地的环境变量的path中

![path](https://git.oschina.net/uploads/images/2016/0219/162817_3643dd8f_99114.png "path")


**安装protobuff**
   
    1.找到下载的工具包中的“全文件压缩包”（protobuf-2.5.0.tar.bz2）和“编译器压缩包”（protoc-2.5.0-win32.zip）。
    2.将“protobuf-2.5.0.tar.bz2”解压。
    3.将“protoc-2.5.0-win32.zip”解压,并将目录添加到本地的环境变量的path中。
    4.在里面创建compiler文件夹，即protobuf-2.5.0\python\google\protobuf\compiler。
    5.将“编译器压缩包”里的protoc.exe拷到protobuf-2.5.0\src。
    6.用命令行进入刚解压的protobuf\python目录，执行命令python setup.py install。

**安装xlrd（xls reader）**

    1.这里是列表文本 找到下载的工具包中的xlrd-0.9.4.tar.gz并解压
    2.用命令行进入刚解压的xlrd目录，执行命令python setup.py install

**xls_deploy_tool.py介绍**

[xls_deploy_tool.py](http://https://github.com/jameyli/tnt/tree/master/python)是腾讯魔方工作室[jameyli](http://https://github.com/jameyli)同学的作品.xls格式请遵循[此格式](https://github.com/jameyli/tnt/blob/master/python/xls_deploy_tool.py)

    使用方法是在命令行键入如下命令
        python xls_deploy_tool.py <表格名> <xls文件名>
    它将“符合格式”的xls文件生成data文件和proto文件
    “符合格式”指的是要求xls的每一列的前4行用于定义数据格式。例子如图：

![数据格式](https://git.oschina.net/uploads/images/2016/0219/184941_6538d7d9_99114.png "数据格式")

    详细格式文档可参考xls_deploy_tool.py 的文件注释。
    
**4. xls_deploy_tool.py生成的文件**

    1. 生成的data文件是最终在游戏内进行读取的数据文件，以protobuff定义的格式进行压缩存储（或者明文存储，但xls_deploy_tool.py暂时只实现了压缩存储）
    2. 生成的proto文件是用于解释上面这个data文件的解释类。

**5. 具体命令行的使用请参考tools/kill.bat**

**插件在项目中的使用**

**1. 目录结构**

![输入图片说明](https://git.oschina.net/uploads/images/2016/0222/100944_b3bdf1cb_99114.png "在这里输入图片标题")

         
ExcelPlugin           插件总目录

     Editor           插件的扩展插件 （具体请看代码）
     Plugins          第三方库文件
     Scripts          插件的基础脚本

Scene                 场景目录

Scripts                脚本

     Killer
        DataConfig
            ProtoGen
                tnt_deploy_goods_info        goods_info数据表的C#代码
                tnt_deploy_goods_info_Ext    goods_info数据表的C#类的扩展

StreamingAssets      Streaming目录  

       DataConfig     配置目录
            DataConfig                       配置目录，需要将所需生成的excel内容添加进去
            tnt_deploy_goods_info            goods_info.excel生成的数据存储文件
            tnt_deploy_hero                  hero.excel生成的数据存储文件
**2. 使用流程（以hero.xls为例）**

    1. 将hero.xls放到tools/DataConfig目录中
    2. 在Assets/StreamingAssets/DataConfig/DataConfig文件中添加hero，最后结果为下图所示

![输入图片说明](http://git.oschina.net/uploads/images/2016/0222/111515_e841845f_99114.png "在这里输入图片标题")

    3.点击unity3d导航栏Editor--ExcelToolEditor,打开编辑器。选中hero,点击生成。
      工具会自动在Assets/Scriptes/Killer/DataConfig/ProtoGen目录生成tnt_deploy_hero.cs和tnt_deploy_hero_Ext.cs
      如果excel有唯一标识，修改_Ext.cs文件中的GetId接口
      还会在Assets/StreamingAssets/DataConfig目录生成tnt_deploy_hero.data存储信息
    4.在代码中使用
          获取所有英雄信息:List<HERO> heros = ProtoDatabase.Instance.GetData<HERO>();
          根据英雄id获取英雄信息:HERO hero = ProtoDatabase.Instance.GetTypeAndId<HERO>(1);